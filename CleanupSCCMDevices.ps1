﻿$installdrive = "C:"
$loglocation = "C:\Temp\Logfiles\"
$maxdevices = 3000

Function Write-Log{            
##----------------------------------------------------------------------------------------------------            
##  Function: Write-Log            
##  Purpose: This function writes trace32 log fromat file to user desktop      
##  Function by: Kaido Järvemets Configuration Manager MVP (http://www.cm12sdk.net)
##----------------------------------------------------------------------------------------------------                            
PARAM(                     
    [String]$Message,                                  
    [int]$severity,                     
    [string]$component                     
    )                                          
    $TimeZoneBias = Get-WmiObject -Query "Select Bias from Win32_TimeZone"                     
    $Date= Get-Date -Format "HH:mm:ss.fff"                     
    $Date2= Get-Date -Format "MM-dd-yyyy"                     
    $type=1                         
    
    "<![LOG[$Message]LOG]!><time=$([char]34)$date+$($TimeZoneBias.bias)$([char]34) date=$([char]34)$date2$([char]34) component=$([char]34)$component$([char]34) context=$([char]34)$([char]34) type=$([char]34)$severity$([char]34) thread=$([char]34)$([char]34) file=$([char]34)$([char]34)>"| Out-File -FilePath "$loglocation\Deleted-Devices-$Date2.Log" -Append -NoClobber -Encoding default            
    }   

Function DisableComputers {
    $DaysInactive = 60 
    $time = (Get-Date).Adddays(-($DaysInactive))
    $DisabledComputersOU = "OU=Disabled Computers,DC=" + ($ADDomain.Domain.Split(".")[0]) + ",DC=" + ($ADDomain.Domain.Split(".")[1])
 
    # Get all AD computers with lastLogonTimestamp less than our time
    $InactiveComputers = Get-ADComputer -Filter {LastLogonTimeStamp -lt $time} -Properties LastLogonTimeStamp

    ForEach ($InactiveComputer in $InactiveComputers) {
    Disable-ADAccount $InactiveComputer
    Move-ADObject -Identity $inactivecomputer.objectguid -TargetPath $DisabledComputersOU
    }
}


#Load Active Directory Module
Import-Module ActiveDirectory

#Load Configuration Manager PowerShell Module
Import-module ($Env:SMS_ADMIN_UI_PATH.Substring(0,$Env:SMS_ADMIN_UI_PATH.Length-5) + '\ConfigurationManager.psd1')

#Get SiteCode
$SiteCode = Get-PSDrive -PSProvider CMSITE
Set-location $SiteCode":"

#Get SCCM Forests
$Forests = Get-CMActiveDirectoryForest

$ADDomain = Get-ADDomainController -Discover -Domain loogman.nl
$DC = $ADDomain.HostName.Value
$DomainName = $ADDomain.Domain.Split(".")[0]
$Cred = Get-Credential $DomainName\asterict


#Name=$Address.Split("@")[0]

if (-not(Get-PSDrive $DomainName)) {
    New-PSDrive `
        –Name $DomainName `
        –PSProvider ActiveDirectory `
        –Server $DC `
        –Credential $Cred `
        –Root ‘//RootDSE/’ `
        -Scope Global
    }Else{
    "Drive already exists"
    }


Set-CMQueryResultMaximum -Maximum $maxdevices
$Computers = Get-CMDevice -CollectionName "All Workstations" | Where {$_.Domain -eq $DomainName} | ForEach-Object {$_.Name}
$Computers = Get-CMDevice -CollectionName "LGM All Workstations" | ForEach-Object {$_.Name}

for( $n = 0; $n -le $Computers.count -1; $n++ ) {
  Remove-Variable rtn
  Set-Location $DomainName":"
  $comp = $Computers[$n] 
  $comp = $comp.ToUpper() 
  $rtn = Get-ADComputer $comp
  IF(!$rtn)  { 
    Set-Location $sitecode":"
    $tempdom = Get-CMDevice -name $Computers[$n] | ForEach-Object {$_.Domain}
    $resourcetype = Get-CMDevice -name $Computers[$n] | ForEach-Object {$_.ResourceType}
    $devicetype = Get-CMDevice -name $Computers[$n] | ForEach-Object {$_.DeviceType}
    if (($tempdom -eq $domainname) -And  ($resourcetype -eq "5") -And ($devicetype -eq $null) ){
    Write-Log -Message "Deleting device from ConfigMgr 2012 - $Comp deleted" -severity 3 -component "Cleanup"
    $comp
    Remove-CMDevice -name $comp -force 
    }
    ELSE
        {
      IF(($tempdom -ne $domainname) -And ($tempdom -ne $null) -And ($devicetype -eq $null)) { Write-Log -Message "Preserving device in ConfigMgr 2012 - $Comp not deleted, member of other domain or workgroup." -severity 1 -component "Cleanup"}
      IF(($tempdom -eq $null) -And ($devicetype -eq $null)) { Write-Log -Message "Preserving device in ConfigMgr 2012 - $Comp not deleted, manually added or mobile device." -severity 1 -component "Cleanup"}
      IF($devicetype -ne $null) { Write-Log -Message "Preserving device in ConfigMgr 2012 - $Comp not deleted, mobile device." -severity 1 -component "Cleanup"}
    }   Write-Output "Yes"
}
}





